MetricPCE
============
Is an extension to the JastAddJ compiler, its main purpose is to calculate metrics on Java object oriented projects. The analyses are done on source code level. The implemented metrics suites are the following: Chidamber & Kemerer object-oriented metrics suite and the Martin metric.

Use these commands to clone the project and build it the first time:

	git clone --recursive git@bitbucket.org:edan70/java-metrics-johan-joel.git
	cd java-metrics-johan-joel
	gradle jar

Make sure to include the `--recursive` in the clone command to get the JastAddJ
submodule.

If you forgot the `--recursive` flag, don't worry, just go into the newly cloned
project, down into the map jastaddj, and run these commands:

	git submodule init
	git submodule update

That should download the JastAddJ git repository into the local directory `jastaddj`.

If you don't have Gradle installed you can use the `gradlew.bat` (on Windows)
or `gradlew` (Unix) script instead. For example to build on Windows run the
following in a command prompt:

	gradlew jar
    
and for Unix

	./gradlew jar

Ignore and configurations
-------------
The file metircpce-ignore.cfg contains lines which specify which packages that should be ignored in the evaluations, add as many you need.

For configurations of the evalutaion proccess inspect the file metricpce-config.cfg. It Contains configurations for both the CK- and Martin-metric, and also some optional configurations for the result printing process.  

IMPORANT!
These files are crutial for running the jar file, they need to be placed in the same directory as the jar file.

Build and Run
-------------
Select a project for evaluation, the example below will be run on projects under the folder "samples".
Which can be run by the following command:
	
	gradle jar
	java -jar java-metrics-johan-joel.jar samples/threePackagesMartin

for a bit larger project try:
	
	java -jar java-metrics-johan-joel.jar samples/JSON-java-master

Classpaths
-------------
To add external jar(s) to the evalutation process add this following line to the run command:
	
	-classpath example/exampledir1/example1.jar:example/exampledir2/example2.jar.

The location of different jar:s needs to be sperated with ":".
The resulting command will look something like this:
	
	java -jar java-metrics-johan-joel.jar example/ -classpath example/exampledir1/example1.jar:example/exampledir2/example2.jar

Results
-------------
Results of the metrics will be saved on files named "metricpce-result.txt", "metricpce-resultclassgraph.dot" and "metricpce-resultpackagegraph.dot", these will be placed in the directory of the project.

metricpce-result.txt will contain all values from the evaluation.
Both .dot files contains graphs with representations of the result, in Graphvis .dot format.
For example metricpce-resultclassgraph.dot can be exported to a pdf, on UNIX, with the following command

	dot -Tpdf metricpce-resultclassgraph.dot > metricpce-resultclassgraph.pdf

This will show all relations between classes in different packages, metricpce-resultpackagegraph will however only show the relation on package level.
