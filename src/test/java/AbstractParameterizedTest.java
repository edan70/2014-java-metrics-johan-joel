package tests;

import java.io.File;
import java.util.Collection;
import java.util.LinkedList;import AST.*;
import org.jastadd.jastaddj.*;
import org.jastadd.util.*;

/**
 * A parameterized test suite. Adds helper methods for
 * parameterized testing.
 */
abstract public class AbstractParameterizedTest extends AbstractTestSuite {

	/**
	 * File extension for test input files. EDIT ME
	 */
	private static final String IN_EXTENSION = ".txt";
	/**
	 * Test output is written to a file with this extension
	 */
	private static final String OUT_EXTENSION = ".out";
	/**
	 * File extension for expected test output
	 */
	private static final String EXPECTED_EXTENSION = ".expected";

	protected final String inDirectory;
	protected final File inFile;
	protected final File outFile;
	protected final File expectedFile;

	/**
	 * @param testDirectory
	 * @param testFile
	 */
	public AbstractParameterizedTest(String testDirectory, String inDirectory, String fileName) {
		super(testDirectory);
		this.inDirectory = inDirectory;
		System.out.println(inDirectory);
		inFile = getTestInFile(fileName);
		outFile = getTestOutputFile(fileName);
		expectedFile = getTestExpectedOutputFile(fileName);
	}

	protected File getTestInFile(String fileName) {
		return new File(inDirectory, fileName);
	}

	protected File getTestOutputFile(String fileName) {
		String simpleName = fileName.substring(0,
			fileName.length()-IN_EXTENSION.length());
		return new File(inDirectory, simpleName+OUT_EXTENSION);
	}

	protected File getTestExpectedOutputFile(String fileName) {
		String simpleName = fileName.substring(0,
			fileName.length()-IN_EXTENSION.length());
		return new File(inDirectory, simpleName+EXPECTED_EXTENSION);
	}

	@SuppressWarnings("javadoc")
	public static Iterable<Object[]> getTestParameters(String testDirectory) {
		Collection<Object[]> tests = new LinkedList<Object[]>();
		File testDir = new File(testDirectory);
		if (!testDir.isDirectory()) {
			throw new Error("Could not find '" + testDirectory + "' directory!");
		}
		for (File f: testDir.listFiles()) {
			if(f.isDirectory())
				tests.add(new Object[] {testDirectory + f.getName()});
		}
		return tests;
	}
}
