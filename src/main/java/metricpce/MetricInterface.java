package metricpce;

import AST.*;

public interface MetricInterface{

	public void performUnitMetric(Config config, MetricResult metricResult, CompilationUnit unit);

	public void performPackageMetric(Config config, MetricResult metricResult, JavaPackage javaPackage);
}
