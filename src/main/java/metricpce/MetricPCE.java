package metricpce;

import AST.*;
import org.jastadd.jastaddj.*;
import org.jastadd.util.*;
import java.util.HashSet;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Set;
import java.util.HashSet;
import java.util.ArrayList;
import java.util.zip.*;
import java.lang.Exception;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.io.OutputStreamWriter;
import java.util.Iterator;
import java.lang.StringBuilder;
import java.io.PrintWriter;
import beaver.*;
import java.util.*;
import java.io.*;
import java.io.FileNotFoundException;

public class MetricPCE extends Frontend {
	public static void main(String args[]){
		int exitCode = new MetricPCE().run(args);
		if (exitCode != 0) {
			System.exit(exitCode);
		}
	}

	private static final String TEST = "-test";

	protected final JavaParser parser;
	protected final BytecodeParser bytecodeParser;
	protected Config config;
	protected MetricResult metricResult;
	private ArrayList<MetricInterface> metrics;
	private String resultFileName;
	private String filePath;
	long lStartTime;
	long lEndTime;
	long difference;

	public static final String FILE_NAME = "metricpce-result.txt";
	/**
	 * Initialize the compiler.
	 */
	public MetricPCE()  {
		super("MetricPCE", JastAddJVersion.getVersion());
		this.config = new Config();

		parser = new JavaParser() {
			@Override
			public CompilationUnit parse(java.io.InputStream is,
					String fileName) throws java.io.IOException,
					beaver.Parser.Exception {
				return new parser.JavaParser().parse(is, fileName);
			}
		};
		bytecodeParser = new BytecodeParser();
	}

	private void readyNewRun(String directory){
		filePath = directory + "/";
		resultFileName = filePath + FILE_NAME;
		metricResult = new MetricResult();

		metrics = new  ArrayList<MetricInterface>();
		metrics.add(new MartinMetric());
		metrics.add(new CKMetric());
	}

	public void addMetric(MetricInterface metric){
		metrics.add(metric);
	}
	
	/**
	 * Run the compiler.
	 * @param args command-line arguments
	 * @return 0 on success, 1 on error, 2 on configuration error, 3 on system
	 */
	public int run(String args[]) {
		int run_result;
		if(args.length <= 0)
			throw new Error("Please specify a folder/file for evaluation");
		run_result = preform(args, args[0]);
		return run_result;
	}

	private int preform(String args[], String directory){
		readyNewRun(directory);
		int exitCode = run(args, bytecodeParser, parser);
		if(metricResult.size() == 0){
			System.out.println("No result, please check for errors or ignore/config file");
			return exitCode;
		}
		if(config.isEnabled("print_result_txt"))
			printResults();
		if(config.isEnabled("print_class_graph"))
			printResultGraph("metricpce-resultclassgraph.dot",program.printClassGraph(config, metricResult));
		if(config.isEnabled("print_package_graph"))
			printResultGraph("metricpce-resultpackagegraph.dot",program.printPackageGraph(config, metricResult));
		return exitCode;
	}

	public void changeConfigFile(String filePath){
		config = new Config(filePath);
	}
	

	public void changeIgnoreFile(String filePath){ //used stric
		program.ignorePath = filePath + program.ignorePath;
	}

	@Override
 	public int run(String[] args, BytecodeReader reader, JavaParser parser) {
		lStartTime = System.currentTimeMillis();
		program.initBytecodeReader(reader);
		program.initJavaParser(parser);

		initOptions();
		int argResult = processArgs(args);
		if (argResult != 0) {
			return argResult;
		}

		Collection<String> files = program.options().files();

		if (program.options().hasOption("-version")) {
			printVersion();
			return EXIT_SUCCESS;
		}

		if (program.options().hasOption("-help") || files.isEmpty()) {
			printUsage();
			return EXIT_SUCCESS;
		}

		Collection<CompilationUnit> work = new LinkedList<CompilationUnit>();

		try {
			for (String file: files) {
				program.addSourceFile(file);
		}

		int compileResult = EXIT_SUCCESS;

		// process source compilation units
		Iterator<CompilationUnit> iter = program.compilationUnitIterator();
		while (iter.hasNext()) {
			CompilationUnit unit = iter.next();
			if(unit == null || unit.myPackage().isIgnore())
				continue;
			work.add(unit);
			if(config.isEnabled("compile_error_checking")){
				int result = processCompilationUnit(unit);
				unit.addYourFuckingStuff();	
				switch (result) {
					case EXIT_SUCCESS:
						break;
					case EXIT_UNHANDLED_ERROR:
						return result;
					default:
						compileResult = result;
				}
			}
		}

		// process library compilation units
		RobustMap<String, CompilationUnit> valueMap = (RobustMap<String, CompilationUnit>) program.getLibCompilationUnitValueMap();
		if (valueMap != null) {
			iter = valueMap.robustValueIterator();
			while (iter.hasNext()) {
				CompilationUnit unit = iter.next();
				if(unit == null || unit.myPackage().isIgnore())
					continue;
				work.add(unit);
				if(config.isEnabled("compile_error_checking")){
					int result = processCompilationUnit(unit);
					unit.addYourFuckingStuff();	
					switch (result) {
						case EXIT_SUCCESS:
							break;
						case EXIT_UNHANDLED_ERROR:
							return result;
						default:
							compileResult = result;
					}
				}
			}
		}

		valueMap = (RobustMap<String, CompilationUnit>) program.getLibCompilationUnitValueMap();
		if (valueMap != null) {
			iter = valueMap.robustValueIterator();
			while (iter.hasNext()) {
				CompilationUnit unit = iter.next();
				if(unit == null || unit.myPackage().isIgnore())
					continue;
				unit.addYourFuckingStuff2();
			}
		}

		if (compileResult != EXIT_SUCCESS) {
			return compileResult;
		}

		for (CompilationUnit unit: work) 
			processNoErrors(unit);
		
		Iterator<JavaPackage> iterPack = program.packageIterator();

		while(iterPack.hasNext()){
			JavaPackage p = iterPack.next();
			for(MetricInterface mi : metrics)
				mi.performPackageMetric(config, metricResult, p);
		}
		
		} catch (Throwable t) {
			System.err.println("Errors:");
			System.err.println("Fatal exception:");
			t.printStackTrace(System.err);
		return EXIT_UNHANDLED_ERROR;
		} finally {
		}
		//some tasks
		lEndTime = System.currentTimeMillis();
	 
		difference = lEndTime - lStartTime;
		System.out.println("Evaluation time: " + difference + " ms ");
		return EXIT_SUCCESS;
}


	@Override
	protected void processNoErrors(CompilationUnit unit) {
		if (unit == null)
			return; 

		for(MetricInterface mi : metrics)
			mi.performUnitMetric(config, metricResult, unit);

	}
	@Override
	protected int processArgs(String[] args) {
		ArrayList<String> list = new ArrayList<String>();
		File folder = new File(args[0]);
		listFilesForFolder(folder, list);
		String[] tempArgs = list.toArray(new String[list.size()]);
		if(args.length > 1)
			program.options().addOptions(Arrays.copyOfRange(args, 1, args.length));
		program.options().addOptions(tempArgs);
		boolean error = false;
		Collection<String> files = program.options().files();
		for (String file: files) {
			if (!new File(file).isFile()) {
				System.err.println("Error: neither a valid option nor a filename: " + file);
				error = true;
			}
		}
		return error ? EXIT_CONFIG_ERROR : EXIT_SUCCESS;
	}

	private void listFilesForFolder(File folder, ArrayList<String> list){
		for (File fileEntry : folder.listFiles()) {
			if (fileEntry.isDirectory()) {
				listFilesForFolder(fileEntry, list);
			} else {
				if(fileEntry.getName().endsWith(".java"))
					list.add(folder.getPath() + "/" + fileEntry.getName());
			}
		}
	}
	
	private void printResults(){ // we might want to implemtn a PrettyPrint 
		System.out.print("Saving result to: " + resultFileName + " ");
		BufferedWriter writer;
		FileWriter fos;
		StringBuilder sb = new StringBuilder();
		try{ 
			PrintWriter w = new PrintWriter(new FileWriter(resultFileName));

			Iterator itrName = metricResult.iterator();
			String fileName = "";
			String metric = "";
			while(itrName.hasNext()){
				fileName = String.valueOf(itrName.next()); 
				sb.append("====== ").append(fileName).append(" ======\n");
				for(Iterator itrMetric = metricResult.get(fileName).keySet().iterator(); itrMetric.hasNext();){
					metric = String.valueOf(itrMetric.next());
					sb.append(metric).append("\t\t\t").append(metricResult.get(fileName).get(metric)).append("\n");
				}
			}
			w.println(sb.toString());
			w.close();
			
		}catch(Exception e){
			System.out.println(" --FAIL");
			e.printStackTrace();
		}
		System.out.println(" --SUCCESS");
	}

	private void printResultGraph(String fileName, String output){
		System.out.print("Saving result to: " + filePath + fileName);
		BufferedWriter writer;
		FileWriter fos;
		try{ 
			PrintWriter w = new PrintWriter(new FileWriter(filePath + fileName));
			w.println(output);
			w.close();
		}catch(Exception e){
			System.out.println(" --FAIL");
			e.printStackTrace();
		}
		System.out.println(" --SUCCESS");
	}
}
