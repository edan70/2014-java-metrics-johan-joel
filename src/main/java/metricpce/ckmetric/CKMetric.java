package metricpce;

import AST.*;
import java.util.LinkedHashMap;

/**
 * Compile Java programs.
 */
public class CKMetric implements MetricInterface {

	public void performPackageMetric(Config config, MetricResult metricResult, JavaPackage javaPackage) {}
		
	public void performUnitMetric(Config config, MetricResult metricResult, CompilationUnit unit) {

		if(!config.isEnabled("CK_enabled"))
			return;

		for(TypeDecl td : unit.getTypeDecls()){
			metricResult.put(td.fullName(), new LinkedHashMap<String, String>());
			LinkedHashMap<String, String> res = metricResult.get(td.fullName());

			if(config.isEnabled("CK_DIT"))
				res.put("DIT", String.valueOf(td.getDepthOfInheritanceTree()));
			if(config.isEnabled("CK_WMC"))
				res.put("WMC", String.valueOf(td.getNumberMethods()));
			if(config.isEnabled("CK_NOC"))
				res.put("NOC", String.valueOf(td.getNumberOfChildren()));
			if(config.isEnabled("CK_RFC"))
				res.put("RFC", String.valueOf(td.getResponseForClass()));
			if(config.isEnabled("CK_LCOM"))
				res.put("LCOM", String.valueOf(td.getLackOfCohesion()));
			if(config.isEnabled("CK_NPM"))
				res.put("NPM", String.valueOf(td.getNumberPublicMethods()));
			if(config.isEnabled("CK_Ca"))
				res.put("Ca", String.valueOf(td.getAfferentCoupling()));
		}
	}	
}
