package metricpce;

import AST.*;
import java.util.LinkedHashMap;

/**
 * Compile Java programs.
 */
public class MartinMetric implements MetricInterface {

	public void performUnitMetric(Config config, MetricResult metricResult, CompilationUnit unit) {}

	public void performPackageMetric(Config config, MetricResult metricResult, JavaPackage javaPackage) {

		if(!config.isEnabled("Martin_enabled"))
			return;

		metricResult.put(javaPackage.getName(), new LinkedHashMap<String, String>());
		LinkedHashMap<String, String> res = metricResult.get(javaPackage.getName());

		int pre = (int) (Integer.parseInt(config.get("Martin_calc_precision")));
		pre = pre > 0 ? pre : 1;

		if(config.isEnabled("Martin_NCI"))
			res.put("NCI",String.valueOf(javaPackage.getNumberOfClassesAndInterFaces()));
		if(config.isEnabled("Martin_Ca")) 
			res.put("Ca",String.valueOf(javaPackage.getAfferentCoupling()));
		if(config.isEnabled("Martin_Ce")) 
			res.put("Ce",String.valueOf(javaPackage.getEfferentCoupling()));
		if(config.isEnabled("Martin_A")) 
			res.put("A",String.valueOf((double)Math.round(javaPackage.getAbstractness()*pre)/pre));
		if(config.isEnabled("Martin_I")) 
			res.put("I",String.valueOf((double)Math.round(javaPackage.getInstability()*pre)/pre));
		if(config.isEnabled("Martin_DMS"))
			res.put("DMS",String.valueOf((double)Math.round(javaPackage.getDistanceFromMainSequence(config.isEnabled("Martin_DMS_normalized"))*pre)/pre));
	}
}
