package metricpce;

import java.util.LinkedHashMap;
import java.util.Iterator;

public class MetricResult{
	private LinkedHashMap<String, LinkedHashMap<String, String>> metricResult;

	public MetricResult(){
		metricResult = new LinkedHashMap<String, LinkedHashMap<String, String>>();
	}

	public LinkedHashMap<String, String> put(String key, LinkedHashMap<String, String> value){
		return metricResult.put(key,value);
	}
	
	public LinkedHashMap<String, String> get(String key){
		return metricResult.get(key);
	}

	public Iterator<String> iterator(){
		return metricResult.keySet().iterator();
	}

	public boolean contains(String key){
		return metricResult.get(key) != null;
	}
	public int size(){
		return metricResult.size();
	}
}
