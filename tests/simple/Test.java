package test;

import test2.Test2;
import test3.Test3;
public class Test extends TestAb{
	public static void main(){
		Test test = new Test();
		test.run();
	}

	public void run(){
		Test2 t = getTest2();
		t.run();
		getName();
	}

	public Test2 getTest2(){
		return new Test2();
	}

	public Test3 getTest3(){
		return new Test3();
	}
}
