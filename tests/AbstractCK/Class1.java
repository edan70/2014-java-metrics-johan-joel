package AbstractPackage;

public class Class1 extends AbstractClass{
	public Class1(){
		super("Class1");
	}

	@Override
	public void exe(){	
		System.out.println("func1: " + Func1());
	}

	private String Func1(){
		return	func2();
	}

	private String func2(){
		return "func2";
	}
}
